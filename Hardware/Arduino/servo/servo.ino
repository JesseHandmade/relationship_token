/* Sweep
  by BARRAGAN <http://barraganstudio.com>
  This example code is in the public domain.

  modified 8 Nov 2013
  by Scott Fitzgerald
  http://www.arduino.cc/en/Tutorial/Sweep
*/

#include <Servo.h>
#include <elapsedMillis.h>

Servo myservo;  // create servo object to control a servo
elapsedMillis timer_servo;

bool manualInput = false;     //true = serial input, false = play effects

const int SERVO_INTERVAL = 15;
bool goingForward = true;

int newPos = -1;
int pos = 0;    // variable to store the servo position

void setup() {
  myservo.attach(7);  // attaches the servo on pin 9 to the servo object
  Serial.begin(9600);
  timer_servo = 0;
}

void loop() {
  if (manualInput) {
    if (Serial.available()) {
      String incomingReading = Serial.readString();
      Serial.print(incomingReading);
      newPos = incomingReading.toInt();
      //    Serial.print("\t");
      //    Serial.println(newPos);
    }
    moveServo(newPos);


  } else {
    moveServoRandom();

  }
}

void moveServo(int requestedPos) {

  if (timer_servo > SERVO_INTERVAL) {
    timer_servo = 0;
    myservo.write(requestedPos);
  }
}


void moveServoRandom() {

  if (timer_servo > 100) {
    timer_servo = 0;
    pos = random(0, 6);
    myservo.write(pos);
  }
}





