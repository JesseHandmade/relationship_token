//float float_example = 123.45
//float float_example2 = 28.01
//byte bytes[4];

float float_example = 123.45;
float float_example2 = 28.01;
byte accelX_array[4];
byte accelY_array[4];

float conversionBackToFloat = 0;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  //  Serial.println(conversionBackToFloat);

}

void loop() {

  float2Bytes(float_example, &accelX_array[0]);
  float2Bytes(float_example2, &accelY_array[0]);
  sendDataToHikey(accelX_array, accelY_array, 27);
  /*
    for (int i = 0; i < 4; i++) {
    Serial.println(bytes[i]);
    }
    Serial.print("and the magic number is ");

    bytesToFloat(&bytes[0], conversionBackToFloat);
    union u_tag {
    byte b[4];
    float fval;
    } u;

    u.b[0] = bytes[0];
    u.b[1] = bytes[1];
    u.b[2] = bytes[2];
    u.b[3] = bytes[3];

    conversionBackToFloat = u.fval;
  */
  //  Serial.println(conversionBackToFloat);
  delay(1000);
}


void float2Bytes(float val, byte* bytes_array) {
  // Create union of shared memory space
  union {
    float float_variable;
    byte temp_array[4];
  } u;
  // Overite bytes of union with float variable
  u.float_variable = val;
  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 4);
}

void sendDataToHikey(byte accelX[4], byte accelY[4], int inPosition) {
  //First send the X-axis accelerometer values, then the Y axis
  Serial.write(accelX, 4);
  Serial.write(accelY, 4);
  //after these, send if the device is in position or moving
  Serial.write(inPosition);
  Serial.println();   //finish with a println to show the end of the message
}

