//motor pins
const int enA = 10;
const int in1 = 9;
const int in2 = 8;
const int SLIDER_PIN = A1;

int newPos = -1;
int previousPos = -2;
int sliderPos = -1;
boolean moveNow = false;

void setup() {
  // set all the motor control pins to outputs
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(SLIDER_PIN, INPUT);
  Serial.begin(9600);

}

void loop() {
  //read serial monitor
  if (Serial.available()) {
    String incomingReading = Serial.readString();
    Serial.print(incomingReading);
    newPos = incomingReading.toInt();
    Serial.print("\t");
    Serial.println(newPos);
  }

  //move if wanted
  if (moveNow) {
    moveToPos(newPos, 200);
  }

  //reset bool if new position found
  if (previousPos != newPos) {
    moveNow = true;
  }
  //reset position so it only moves when it has not reached the wanted position
  previousPos = newPos;
}

void moveToPos(int wantedPos, int wantedSpeed) {
  //move forward/backward or stop once destination has been reached
  if (sliderPos > wantedPos + 5) {
    moveBackward();
    setMotorSpeed(wantedSpeed);
    readSliderPos();

  } else if (sliderPos < wantedPos - 5) {
    moveForward();
    setMotorSpeed(wantedSpeed);
    readSliderPos();

  } else {
    stopMotor();
    moveNow = false;
    readSliderPos();
  }
}


/*-------------------------------------------------------

                  LINEAR MOTOR FUNCTIONS

  -------------------------------------------------------*/
void moveForward() {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
}

void moveBackward() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
}

void stopMotor() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
}

void setMotorSpeed(int wantedSpeed) {     //0-255
  analogWrite(enA, wantedSpeed);
}

void readSliderPos() {
  sliderPos = analogRead(SLIDER_PIN);
  Serial.println(sliderPos);
}

