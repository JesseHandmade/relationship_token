#include "communication.h"

/*--------------------------------------------------------------------------
   --------------------------------------------------------------------------
   Contains all functions needed for communication from Hikey to Teensy
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------*/


/*------------------
       SEND
  ------------------*/
// Convert float values to byte array by using the same memory location
// Call function as follows floatToBytes(floatName, &byteArrayName[0]);

void floatToBytes(float val, byte* bytes_array) {
  // Create union of shared memory space
  union {
    float float_variable;
    byte temp_array[4];
  } u;

  // Story float value in union memory, meaning the bytes also have this value.
  u.float_variable = val;

  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 4);
}

bool prevOnTable = false;

void sendDataToHikey(byte accelX[4], byte accelY[4], int inPosition, int onTable) {
  //First send the X-axis accelerometer values, then the Y axis

  //  long currentMillis = millis();
  //  LOG_SERIAL.print(currentMillis);
  //  LOG_SERIAL.print("AccelX = ");
  //  LOG_SERIAL.print(accelX[0]);
  //  LOG_SERIAL.print(", ");
  //  LOG_SERIAL.print(accelX[1]);
  //  LOG_SERIAL.print(", ");
  //  LOG_SERIAL.print(accelX[2]);
  //  LOG_SERIAL.print(", ");
  //  LOG_SERIAL.print(accelX[3]);
  //  LOG_SERIAL.print("\t");
  //  LOG_SERIAL.print("AccelY = ");
  //  LOG_SERIAL.print(accelY[0]);
  //  LOG_SERIAL.print(", ");
  //  LOG_SERIAL.print(accelY[1]);
  //  LOG_SERIAL.print(", ");
  //  LOG_SERIAL.print(accelY[2]);
  //  LOG_SERIAL.print(", ");
  //  LOG_SERIAL.print(accelY[3]);
  //  LOG_SERIAL.print("\t");
  //  LOG_SERIAL.print("inPosition = ");
  //  LOG_SERIAL.print(inPosition);
  //  LOG_SERIAL.print("\t");
  //  //    if (onTable == 0) {
  //  LOG_SERIAL.print("onTable = ");
  //  LOG_SERIAL.println(onTable);
  //  LOG_SERIAL.println();
  //  }

  HIKEY_SERIAL.write(accelX, 4);
  HIKEY_SERIAL.write(accelY, 4);
  //after these, send if the device is in position or moving
  HIKEY_SERIAL.write(inPosition);       
  // LOG_SERIAL.write(inPosition);
  
  // onTable: 0 -> 3     1 -> 4
  if (onTable == 0) {
    HIKEY_SERIAL.write(3);
  } else if (onTable == 1) {
    HIKEY_SERIAL.write(4);
  }
  HIKEY_SERIAL.println();   //finish with a println to show the end of the message
}

