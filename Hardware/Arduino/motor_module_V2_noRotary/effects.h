/*************************************************************************************

   Stores the effects and their sequences for easy reach,- and editability
   Also stores effect speedProfiles

*************************************************************************************/


int speedProfiles[][3] = {
  {8000, 250 * 180, 330} ,              // 0  default
  {120 * 100, 167 * 180, 50 * 10} ,     // 1 Celebrate//
  {120 * 100, 50 * 180, 50 * 10} ,      // 2 thinking   {120 * 100, 50 * 180, 50 * 10} ,      // 2 thinking
  {9500, 6120, 290},                    // 3 To adjust
  {209 * 40, 167 * 300, 1},             // 4 nudge
  {146 * 100, 91 * 180, 30 * 10},       // 5 zoom in/out
  {80 * 100, 120 * 180, 1 * 10},         // 6 move to an angle
  {150 * 100, 40 * 180, 50 * 10},       // 7 spin
  {250 * 100, 140 * 180, 33 * 10},      // 8 celebrate + wake-up spin
  {10 * 100, 6 * 180, 2 * 10},          // 9 jiggly wiggly
  {112 * 100, 74 * 180, 13 * 10},        // 10 thinking full circle
  {140 * 100, 90 * 180, 15 * 10},        // 11 continuous spin
  {70 * 100, 60 * 180, 15 * 10},      // 12 continuous spin slow
  {10, 10, 10}
};

int celebrate_profiles[] = {1, 1, 1, 8, 1, 1, 1, 6};
int celebrate_moves[] = {25, -25, 25, -225, 25, -25, 25, 50};
int celebrate_delays[] = {0, 200, 200, 0, 200, 200, 200, 0};
int celebrate_length = 8;

int thinking_profiles[] = {2, 9, 2, 9, 10, 4, 4};
int thinking_moves[] = {45, 5, 60, 5, 295, 8, -8};
int thinking_delays[] = {0, 0, 0, 0, 0, 0, 0};
int thinking_length = 7;

int confirm_profiles[] = {3, 4, 4};
int confirm_moves[] = {30, -8, 8};
int confirm_delays[] = {0, 200, 0};
int confirm_length = 3;

int nudge_profiles[] = {4, 4};
int nudge_moves[] = {6, -6};
int nudge_delays[] = {0, 0};
int nudge_length = 2;

int wake_up_profiles[] = {7};
int wake_up_moves[] = {400};
int wake_up_delays[] = {0};
int wake_up_length = 1;

int zoom_in_profiles[] = {3};
int zoom_in_moves[] = {200};
int zoom_in_delays[] = {0};
int zoom_in_length = 1;

int detailed_zoom_in_profiles[] = {3, 6};
int detailed_zoom_in_moves[] = {200, 50};
int detailed_zoom_in_delays[] = {0, 0};
int detailed_zoom_in_length = 2;

int zoom_out_profiles[] = {3};
int zoom_out_moves[] = { -200};
int zoom_out_delays[] = {0};
int zoom_out_length = 1;

int spin_profiles[] = {7};
int spin_moves[] = {600};
int spin_delays[] = {0};
int spin_length = 1;

int accept_profiles[] = {1, 1, 1, 1};
int accept_moves[] = {40, -40, 40, -40};
int accept_delays[] = {0, 200, 200, 200};
int accept_length = 4;

int active_behaviour_profiles[] = {12};
int active_behaviour_moves[] = {200};
int active_behaviour_delays[] = {0};
int active_behaviour_length = 1;

int fake_profile[] = {0};
int fake_moves[] = {0};
int fake_delays[] = {0};
int fake_lenght = 1;

int CCW_profile[] = {6};
int CCW_moves[] = {130};

