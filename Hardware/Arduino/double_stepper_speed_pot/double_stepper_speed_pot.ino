
#include <Stepper.h>

const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
// for your motor


// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);
Stepper myStepper2(stepsPerRevolution, 7, 6, 5, 4);

int stepCount = 0;  // number of steps the motor has taken

void setup() {
  // nothing to do inside the setup
  Serial.begin(9600);
  Serial.println("START");
}

void loop() {
  // read the sensor value:
  int sensorReading = analogRead(A0);
  int sensorReading2 = analogRead(A1);
  
  // map the values to motor values
  int motorSpeed = map(sensorReading, 0, 1023, -200, 200);
  if (motorSpeed < 20 && motorSpeed > -20) {
    motorSpeed = 0;
  }
  int motorSpeed2 = map(sensorReading2, 0, 1023, -200, 200);
  if (motorSpeed2 < 20 && motorSpeed2 > -20) {
    motorSpeed2 = 0;
  }

  // set the motor speed for both motors
  if (motorSpeed > 0) {
    myStepper.setSpeed(motorSpeed);
    myStepper.step(1);
    Serial.println(motorSpeed);
  } else if (motorSpeed < 0) {
    myStepper.setSpeed(-motorSpeed);
    myStepper.step(-1);
  }

  if (motorSpeed2 > 0) {
    myStepper2.setSpeed(motorSpeed2);
    myStepper2.step(1);
  } else if (motorSpeed2 < 0) {
    myStepper2.setSpeed(-motorSpeed2);
    myStepper2.step(-1);
  }
}


