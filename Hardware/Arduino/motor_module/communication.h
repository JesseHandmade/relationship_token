
/*--------------------------------------------------------------------------
   --------------------------------------------------------------------------
   Contains all functions needed for communication from Hikey to Teensy
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------*/


/*------------------
       SEND
  ------------------*/
// Convert float values to byte array by using the same memory location
// Call function as follows floatToBytes(floatName, &byteArrayName[0]);

void floatToBytes(float val, byte* bytes_array) {
  // Create union of shared memory space
  union {
    float float_variable;
    byte temp_array[4];
  } u;

  // Story float value in union memory, meaning the bytes also have this value.
  u.float_variable = val;

  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 4);
}


void sendDataToHikey(byte accelX[4], byte accelY[4], int inPosition) {
  //First send the X-axis accelerometer values, then the Y axis
  Serial.write(accelX, 4);
  Serial.write(accelY, 4);
  //after these, send if the device is in position or moving
  Serial.write(inPosition);
  Serial.println();   //finish with a println to show the end of the message
}



/*------------------
       RECEIVE
  ------------------*/
void readSerialMessage(int RPM, int receivedStep) {
  if (Serial.available() > 0) {
    RPM = Serial.read();
    receivedStep = Serial.read();
  }
}
