/*
   This file contains the latest updates for the motor unit of the Daimler token project
   The motor is controlled by a DRV8834 motor driver from Pololu
    It's position is measured with the AMS5084A rotary encoder
    Apart from this the Teensy will also read a gyro
*/

#include "DRV8834.h"
#include <Arduino.h>
#include "AS5048A.h"
#include "BasicStepperDriver.h"
#include "communication.h"
#include <elapsedMillis.h>
#include "MPU9250.h"

//initialize instances of classes usedx
elapsedMillis timer_send_serial_data;
AS5048A angleSensor(10);
MPU9250 IMU(22);

//int currentValue = 0;
int values[] = {0, 0, 0};
byte pitch_array[4];
byte roll_array[4];


// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
#define MOTOR_STEPS 200
int RPM = 60;

#define DIR 17
#define STEP 16
#define ENABLE 14
DRV8834 stepper(MOTOR_STEPS, DIR, STEP, ENABLE);
const int ALLOWED_STEP_OFFSET = 2;

int microSteps = 32;

int currentAngle = 0;
int wantedAngle = 100;
int receivedStep = 100;
int previousReceivedStep = 100;
int deltaAngle = 1;
int acceleration = 1000;
int deceleration = 800;
int extraParameter = 1;

bool stopMoving = false;
bool inPosition = false;
bool startNewMove = false;

void setup() {
  stepper.begin(RPM);
  stepper.enable();
  stepper.setMicrostep(microSteps);   // Set microstep mode (1, 2, 4, 8 , 16 or 32);

  //set the speed profile of the stepper,
  //LINEAR_SPEED allows for acceleration and deceleration
  //CONSTANT_SPEED provides a constant speed throughout the motion
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, acceleration, deceleration);

  Serial.begin(115200);
  angleSensor.init();   //rotary encoder
  //  IMU.start();      //accelerometer
}

/****************************************
 ****************************************

                Main loop

 ****************************************
 ****************************************/

void loop() {
  //Check if the data is fresh or not, move accordingly or play effect (again)
  if (startNewMove) {
    startNewMove = false;    //data has been processed, so reset
    //based on incoming data set new profile and convert the angle if necessary or play an effect
    if (receivedStep > 0 && receivedStep < 200) {
      wantedAngle = convertStepsToDeg(receivedStep);
      setNewSpeedProfile(RPM, acceleration * 10, deceleration * 10);
    } else {
      playEffect(receivedStep);
      if (stepper.interruptAction) {
        wantedAngle = getCurrentAngle();
        stepper.interruptAction = false;
      }
    }
  }

  //check where the motor is now, and move if necessary, only when not playing an effect
  //The motor does not have to be on the perfect angle, the user will not notice this
  //So, we allow a little offset to make positioning the motor easier
  if (currentAngle >= wantedAngle - ALLOWED_STEP_OFFSET && currentAngle <= wantedAngle + ALLOWED_STEP_OFFSET && playEffect == false) {
    inPosition = true;
  } else {
    inPosition = false;
  }

  //Check if the motor is in position, if not, move to that position
  if (!inPosition) {
    currentAngle = getCurrentAngle();                         //update again to be sure we have the latest value
    calculateShortestAngle(currentAngle, wantedAngle);        //find the shortest path to the wanted angle
    stepper.move(convertDegToSteps(deltaAngle)*microSteps);   //move to the new position (in steps)
  }

  //send data every x seconds to keep serial port from flooding
  //  if (timer_send_serial_data > 10) {
  //  IMU.Update();
  //  Serial.print("\t");
  //  Serial.print(IMU.pitch);
  //  Serial.print("\t");
  //  Serial.println(IMU.roll);
  //    //    //Send data to the app running on the Hikey board
  //    //    //    floatToBytes(IMU.pitch, pitch_array);
  //    //    //    floatToBytes(IMU.roll, roll_array);
  //    //    sendDataToHikey(pitch_array, roll_array, 0);
  //
  //    timer_send_serial_data = 0;
  //  }
}


/********************

    ROTARY ENCODER

 ********************/
word val;
word prevVal;

int getCurrentAngle() {

  //sometimes the rotary encoder gives a 0-reading (never twice in a row)
  //to prevent from using this strange value for calculation, read twice to make sure that the 0 is not an error
  prevVal = val;
  val = angleSensor.getRawRotation();
  if (val == 0) {
    val = prevVal;
  }
  if (val == 0 && prevVal != 0) {
    val = angleSensor.getRawRotation();
  }
  int angle = (val * 360.00) / 16384.00;
    Serial.print("\t");
    Serial.print(angle);
    if (angleSensor.getErrors() > 0) {
      Serial.print("Errors: ");
      Serial.print(angleSensor.getErrors());
    }
  return angle;
}

void calculateShortestAngle(int a0, int a1) {
  int max = 360;
  int da = (a1 - a0) % max;
  deltaAngle = 2 * da % max - da;
}

int convertStepsToDeg(int stepsReceived) {
  int angle = int(receivedStep * 1.8);
  return angle;
}

float convertDegToSteps(int deg) {
  float steps = ((float)deg / 360.0) * 200.0;
  return steps;
}

/********************

    COMMUNICATION

 ********************/
void serialEvent() {
  if (Serial.available() > 4) {
    receivedStep = Serial.read();
    acceleration = Serial.read();
    deceleration = Serial.read();
    RPM = Serial.read();
    extraParameter = Serial.read();

    if (acceleration == 0) {
      acceleration = 10;
    }
    if (deceleration == 0) {
      deceleration = 10;
    }
    stepper.interruptAction = true;
    startNewMove = true;
  }
}

void setNewSpeedProfile(int RMP_param, int acceleration_param, int deceleration_param) {
  stepper.setRPM(RMP_param);
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, acceleration_param, deceleration_param);
}

void playEffect(int effectNumber) {
  if (effectNumber == 202) {
    effect_wake_up();                               //D200 - r001
  } else if (effectNumber == 203) {
    effect_zoom_in();                               //D010 - e001
    //subtle difference between zoom in and out is with the start of the animation, making a different sound
  } else if (effectNumber == 204) {
    effect_zoom_out();                              //D010 - e002
  } else if (effectNumber == 205) {
    effect_nudge();
  } else if (effectNumber == 206) {
    effect_confirm_preview();                       //D100 - r001
  } else if (effectNumber == 207) {
    effect_celebrate();                             //C100 - r002
  } else if (effectNumber == 208) {
    effect_thinking();                              //S100 - r001
  }
}


/**************************************************

     has all the effect that the motor can play

 **************************************************/
void effect_wake_up() {
  //deceleration needs to be low in order for the object not to rotate too much when on the table
  stepper.setRPM(120);
  //do we need to keep the rythm?
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, 920, 520);
  stepper.move(40 * microSteps);
  //      delay(3);
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, 920, 520);
  stepper.move(40 * microSteps);
  //      delay(3);
  stepper.move(40 * microSteps);
  //      delay(extraParameter);
  stepper.move(40 * microSteps);
  //      delay(extraParameter);
  stepper.move(40 * microSteps);
}

void effect_zoom_in() {
  stepper.setRPM(75);
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, 650,  350);
  stepper.move(200 * microSteps);
}

void effect_zoom_out() {
  stepper.setRPM(75);
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, 860,  350);
  stepper.move(-200 * microSteps);
}

void effect_nudge() {
  stepper.setRPM(120);
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, 700, 1000);
  int stepsNudge = random(3, 10);
  stepper.move(stepsNudge * microSteps);
  stepper.move(-stepsNudge * microSteps);
}

void effect_confirm_preview() {
  stepper.setRPM(184);
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, 540, 800);
  stepper.move(100 * microSteps);

  //add 180 degrees to wantedstep, so the motor stays at the wanted position
  if (wantedAngle > 180) {
    wantedAngle = wantedAngle - 180;
  } else if (wantedAngle <= 180) {
    wantedAngle = wantedAngle + 180;
  }
}

void effect_celebrate() {
  // move around to celebrate, move CCW because the design also zooms out a layer
  setNewSpeedProfile(RPM, 950, 900);
  stepper.move(25 * microSteps);
  stepper.move(-25 * microSteps);
  stepper.move(25 * microSteps);

  setNewSpeedProfile(RPM, 750, 600);
  stepper.move(-225 * microSteps);

  setNewSpeedProfile(150, 950, 600);
  stepper.move(25 * microSteps);
  stepper.move(-25 * microSteps);
  stepper.move(25 * microSteps);


  //  setNewSpeedProfile(RPM, 950, 900);
  //  stepper.move(extraParameter * microSteps);
  //  stepper.move(-extraParameter * microSteps);
  //  stepper.move(extraParameter * microSteps);
  //
  //  setNewSpeedProfile(RPM, 750, 600);
  //  stepper.move(-220 * microSteps);
  //
  //  setNewSpeedProfile(150, 950, 600);
  //  stepper.move(extraParameter * microSteps);
  //  stepper.move(-extraParameter * microSteps);
  //  stepper.move(extraParameter * microSteps);


  //  setNewSpeedProfile(RPM, 950, 900);
  //  stepper.move(80 * microSteps);
  //  stepper.move(-80 * microSteps);
  //  stepper.move(80 * microSteps);
  //
  //
  //  setNewSpeedProfile(RPM, 750, 600);
  //  stepper.move(-280 * microSteps);
  //

}

void effect_thinking() {
  //look around
  stepper.setRPM(100);
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, 400, 650);
  stepper.move(random(20, 50)* microSteps);
  delay(random(100, 300));
  stepper.move(random(-20, -80) *microSteps);
  delay(random(100, 300));

  //make sure to end up at the same place where it started
  getCurrentAngle();
  //  if (deltaAngle < 0) {
  //    deltaAngle = 180 + deltaAngle;
  //  }
  calculateShortestAngle(currentAngle, wantedAngle);
  stepper.setRPM(75);
  stepper.setSpeedProfile(DRV8834::Mode::LINEAR_SPEED, 550, 800);
  stepper.move(convertDegToSteps(deltaAngle)*microSteps);

  //confirmation nudge
  effect_nudge();
}
