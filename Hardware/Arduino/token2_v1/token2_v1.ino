#include "AS5048A.h"
#include "MPU9250.h"
#include "StepControl.h"
#include "communication.h"
#include "effects.h"
#include <Arduino.h>
#include <elapsedMillis.h>
elapsedMillis timer_send_data; //send data on this interval
elapsedMillis timer_calibration; //time to calibrate between each round
elapsedMillis effects_delay; //variable timer for delays between parts of effects
elapsedMillis timer_check_if_held; //timer to check if the device is still in hand
elapsedMillis timer_active_behaviour; //time between active behaviour loops
elapsedMillis timer_calibration_on_table; //timer to recalibrate the accel

// #define ANGLESENSOR
#ifdef ANGLESENSOR
AS5048A angleSensor(ENC_CS_PIN);
#endif // DEBUG
//initiate all controllers and sensors

MPU9250 IMU(IMU_INT_PIN);
Stepper motor;
StepControl<> controller;

//constants motor
#define MOTOR_STEPS 200 //amount of steps of the motor
const int MICRO_STEPS = 32; //microsteps per step

//timer
const int UPDATE_FREQUENCY = 50; //frequency to send data to the main app
const int ACTIVE_EFFECT_DELAY = 7000; //effects delay during active move

//Detection if device is on table or in hand
const float IN_HAND_THRESHOLD = 0.15; //accelerometer delta to check if token is in hand or on table
const float IN_HAND_THRESHOLD_IMMEDIATE = 10; //angle in which the device is seen as in hand
const float IN_HAND_SPIKE = 0.7; //accel delta to check if the device is pick up (higher than in hand)
const int ON_TABLE_TIME = 60000; //delay to check if table is really on table
bool givenMessage = false; //makes sure a message of in hand/on table is send just once
bool onTable = true; //to check if device is on table or in hand
byte buf[6]; //buffer for serial data input

//to calculate in position or not
const int ANGLE_OFFSET = 275; //difference between digital and actual position is an ANGLE
/*275 for HM token      250 for token 1      75 for token 2*/
const int ALLOWED_STEP_OFFSET = 3; //delta of motor that is acceptable when moving to angle
int currentAngle = 0; //
int wantedAngle = 100 + ANGLE_OFFSET; //the new angle to go to
int deltaAngle = 1; //to store delta between wanted and currenange
bool reachedWantedPos = true; // check if the wanted position has been reached, so motor can be switched off

//for reading serial data
byte receivedStep = 100; //received step or effect from the android app
byte receivedProfile = 6; //received speed profile from the app
int acceleration = 250; //received acceleration profile (only when using the processing tool)
int pullInSpeed = 1; //received starting speed (only when using the processing tool)
int extraParameter = 1; //received extra usable parameter

//for sending data to the main app
bool communicateWithApp = true; //set to true to set the correct serialEvent and send data to the app
byte pitch_array[4]; //stores pitch values of accelerometer
byte roll_array[4]; //stores roll values of accelerometer

bool inPosition = false; //to check if the motor is in the wanted position
bool startNewMove = false; //to check if a new message contains new information
bool counterStopped = false; //To check if the motion has reached to position to print only once

//for calibration
bool calibrated = false; //checks for calibration or not
int calibrationRounds = 100; //amount of times the accel gets read before checking values
int calibrationIndex = 0; //index for above
float correctionPitch = 0; //corrects the offset that can exist after booting
float correctionRoll = 0; //corrects the offset that can exist after booting
float prevPitch = 0; //stores value of pich for comparison during next loop
float prevRoll = 0; //stores value of roll for comparison during next loop
bool recalibrated = false; //check if the accel needs recalibration, so it knows that a sudden change is not user input

//for playing effects
int active_effect_length; //stores the amount of moves of the active effect
int* active_profiles; //stores the profiles of the active effect
int* active_moves; //stores the moves of the active effect
int* active_delays; //stores the delays between the active effects
int effect_play_index = 0; //index for active effects
bool playingEffect = false; //to check if an effect is being played or not
bool playingActiveEffect = false; //exception to the effects is active behaviour, check if playing or not

// SHAKE DETECTION
elapsedMillis timer_shakeSampleRate;
elapsedMillis timer_shakeDebounce;
elapsedMillis timer_shakeRepeat;
int shakeDebounceDelay = 170;
int shakeDebounceEnd = 500;
int shakeRepeatDelay = 500;
#define SHAKE_THRESHOLD 5
bool shakeDetect = false;
int shakeState = 0;
bool shaken = false;

void setup()
{
    LOG_SERIAL.begin(115200);
    HIKEY_SERIAL.begin(115200);
    LOG_SERIAL.println("Serial ports initialized.");

    IMU.start();
#ifdef ANGLESENSOR angleSensor.init(); //rotary encoder
#endif
    LOG_SERIAL.println("Setup done");
}

/********************************************************************************************************************************************/

void loop()
{
    if (!calibrated) {
        if (timer_calibration > 50) {
            IMU.Update();
            // IMU.printYawPitchRoll();
            calibrationIndex++;

            //check if the calibration has enough rounds, so the
            if (calibrationIndex > calibrationRounds) {
                calibrated = true;
                IMU.printYawPitchRoll();
                correctionPitch = IMU.pitch;
                correctionRoll = IMU.roll;
                onTable = true;
                LOG_SERIAL.println("Done calibrating");
            }
            timer_calibration = 0;
        }
    } else {
        /*************************************
          Update received messages
    *************************************/
        if (startNewMove) {
            reachedWantedPos = false;
            if (controller.isRunning()) {
                controller.stopAsync();
                resetSequences();
            }

            //    based on incoming data set new profile and convert the angle if necessary or play an effect
            if (receivedStep >= 0 && receivedStep < 200) { //new angle to move to, set up everything for the move
                //        currentAngle = getCurrentAngle() + ANGLE_OFFSET;
                wantedAngle = convertStepsToDeg(receivedStep + ANGLE_OFFSET);
                LOG_SERIAL.println("received Angle");
            } else { //if it is above 200, the info received is an effect to be played
                playingEffect = true;
                playEffect(receivedStep);
                LOG_SERIAL.println("received an effect");
            }
            startNewMove = false; //data has been processed, so reset the startmove
        }

        /*******************************************************************************
         Check if effect consists of more than one move and react accordingly
    *******************************************************************************/

        //check if the active effect is running or not
        if (playingActiveEffect) {
            if (timer_active_behaviour > ACTIVE_EFFECT_DELAY) {
                timer_active_behaviour = 0;
                enableMotor();
                resetSequences();
                playEffect(210);
                playingEffect = true;
            }
        }

        //check if there are more parts of the effects to be played
        if (playingEffect) {
            if (!controller.isRunning() && effect_play_index < active_effect_length) {
                unsigned int currentDelay = active_delays[effect_play_index];
                if (effects_delay > currentDelay || currentDelay == 0) {
                    LOG_SERIAL.println("Playing next move");
                    playNextMoveOfEffect();
                    updateSequences();
                }
            } else {
                if (!controller.isRunning() && effect_play_index >= active_effect_length) {
                    resetSequences();
                }
            }
        }

        //within the active effect, the motor only needs to be on while playing the effect
        if (playingActiveEffect) {
            if (controller.isRunning() == false) {
                disableMotor();
            }
        }

        /*************************************
           Move/correct position
    *************************************/
        //check if the motor is in position or not
        if (!controller.isRunning()) {
#ifdef ANGLESENSOR
            currentAngle = getCurrentAngle() + ANGLE_OFFSET;
            deltaAngle = calculateShortestAngle(currentAngle, wantedAngle);
            //      LOG_SERIAL.print("deltaAngle = ");
            //      LOG_SERIAL.println(deltaAngle);

            //if the angles are close to each other, the motor is in position
            if (deltaAngle > ALLOWED_STEP_OFFSET || deltaAngle < -ALLOWED_STEP_OFFSET) {
                inPosition = false;
            } else {
                inPosition = true;
            }
#endif
        }

        //set to wantedAngle if not in position and not playing an effect
        if (!inPosition && !playingEffect && !controller.isRunning() && !reachedWantedPos) {
            counterStopped = false;
            setPredefinedSpeedProfile(receivedProfile);
            currentAngle = getCurrentAngle() + ANGLE_OFFSET;
            deltaAngle = calculateShortestAngle(currentAngle, wantedAngle);

            //      LOG_SERIAL.print("wantedAngle = ");
            //      LOG_SERIAL.print(wantedAngle);
            //      LOG_SERIAL.print("\t");
            //      LOG_SERIAL.print("currentAngle = ");
            //      LOG_SERIAL.print(currentAngle);
            //      LOG_SERIAL.print("\t");
            //      LOG_SERIAL.print("deltaAngle = ");
            //      LOG_SERIAL.println(deltaAngle);

            moveToStep(convertDegToSteps(deltaAngle) * MICRO_STEPS);

            LOG_SERIAL.println("I AM MOVING");

        } else if (inPosition && !playingEffect) {

            controller.stopAsync(); //stop the movement when position reached, gets rid of jittery movements around the wantedangle
            disableMotor();
            if (!counterStopped) {
                counterStopped = true;
                LOG_SERIAL.println("I am in position");
                LOG_SERIAL.println();
            }
            reachedWantedPos = true;
        }

        /*************************************
              Read AccelData
    *************************************/

        IMU.Update(); //update as fast as possible (so every loop)
        shakeDetection();
        float pitch = IMU.pitch - correctionPitch;
        float roll = IMU.roll - correctionRoll;
        //    LOG_SERIAL.print("pitch = ");
        //    LOG_SERIAL.print(pitch);
        //    LOG_SERIAL.print("roll = ");
        //    LOG_SERIAL.println(roll);

        if (timer_send_data > UPDATE_FREQUENCY) {
            //      LOG_SERIAL.print("pitch Delta: ");
            //      LOG_SERIAL.print(abs(prevPitch - pitch));
            //      LOG_SERIAL.print("\t");
            //      LOG_SERIAL.print("roll Delta: ");
            //      LOG_SERIAL.println(abs(prevRoll - roll));

            //check the amount of movement between last and current round to establish if the device moves by the user or not
            if (!playingEffect) {
                //if the token is rotated more than a certain angle, it is certainly in the hand of the user
                if (abs(roll) > IN_HAND_THRESHOLD_IMMEDIATE || abs(pitch) > IN_HAND_THRESHOLD_IMMEDIATE) {
                    onTable = false;
                    //          enableMotor();
                    // LOG_SERIAL.println("the token is picked up - angle");
                } else if (abs(prevRoll - roll) > IN_HAND_SPIKE || abs(prevPitch - pitch) > IN_HAND_SPIKE) {
                    if (recalibrated) {
                        recalibrated = false;
                        LOG_SERIAL.println("Ignored a spike pickup after recalibration");
                    } else {
                        onTable = false;
                        //            enableMotor();
                        // LOG_SERIAL.println("the token is picked up - spike");
                    }
                }

                //reset the timer during effects, as these should only happen during or simulate user interaction
                if (controller.isRunning()) {
                    timer_check_if_held = 0;
                    //          LOG_SERIAL.println("reset timer ");
                }

                if (!onTable) {
                    //          LOG_SERIAL.println(timer_check_if_held);
                    if ((abs(prevRoll - roll) > IN_HAND_THRESHOLD || abs(prevPitch - pitch) > IN_HAND_THRESHOLD)) {
                        //not on table so reset
                        // LOG_SERIAL.println("Timer is reset");
                        timer_check_if_held = 0;
                        givenMessage = false;
                    }
                }

                //check if the device is not moving for a while --> assume it is on table
                if (timer_check_if_held > ON_TABLE_TIME) {
                    if (!givenMessage) {
                        LOG_SERIAL.println("ON THA TABLE");
                        onTable = true;
                        disableMotor();
                        givenMessage = true;
                    }
                }

                //if on table for a while, recalibrate the accelerometer
                if (onTable) {
                    if (timer_calibration_on_table > 2000) {
                        recalibrateAccel();
                        timer_calibration_on_table = 0; //keep on resetting every 2 seconds, for calibration close to pick up
                    }
                } else {
                    timer_calibration_on_table = 0;
                }
            }

            //reset values for next round
            prevRoll = roll;
            prevPitch = pitch;

            /*************************************
             Send data to application
      *************************************/
            if (communicateWithApp) {
                // 10 bytes total, pitch (4), roll (4), if inPosition (1), onTable(1)    (0 is false, 1 is true);
                //        LOG_SERIAL.print("\t");
                //        LOG_SERIAL.print("pitch = ");
                //        LOG_SERIAL.print(pitch);
                //        LOG_SERIAL.print("\t");
                //        LOG_SERIAL.print("roll = ");
                //        LOG_SERIAL.print(roll);
                //        LOG_SERIAL.print("\t");
                //        LOG_SERIAL.print("inPosition = ");
                //        LOG_SERIAL.print(inPosition);
                //        LOG_SERIAL.print("\t");

                floatToBytes(pitch, pitch_array);
                floatToBytes(roll, roll_array);
                //        LOG_SERIAL.print("OnTable = ");
                //        LOG_SERIAL.print(onTable);
                //        LOG_SERIAL.println();
                if (playingActiveEffect) {
                    onTable = false;
                }
                sendDataToHikey(pitch_array, roll_array, inPosition, onTable, shaken);
                shaken = false;
                timer_send_data = 0;
            }

            timer_send_data = 0; //reset timer to initiate new cycle

            if (communicateWithApp) {
                if (HIKEY_SERIAL.available() >= 6) {
                    HIKEY_SERIAL.readBytes(buf, 6);
                    if (buf[0] == 0xDE && buf[1] == 0xAD && buf[4] == 0xBE && buf[5] == 0xEF) {
                        enableMotor();
                        playingActiveEffect = false;
                        reset_held_timer();
                        LOG_SERIAL.print("ReceivedStep: ");
                        LOG_SERIAL.println(buf[2]);
                        LOG_SERIAL.print("profile: ");
                        LOG_SERIAL.println(buf[3]);
                        receivedStep = buf[2];
                        receivedProfile = buf[3];
                        startNewMove = true;
                    } else {
                        HIKEY_SERIAL.readStringUntil('\n');
                        //          LOG_SERIAL.println("Skipping a line of crap");
                    }
                }
            }
        }
    }
}

/********************************************************************************************************************************************************************************************************/
void shakeDetection()
{

    if (timer_shakeRepeat > shakeRepeatDelay) {

        float acceleration = sqrt(pow(IMU.ax, 2) + pow(IMU.ay, 2) + pow(IMU.az, 2)) - 10;

        // LOG_SERIAL.print(IMU.ax); LOG_SERIAL.print(" ");
        // LOG_SERIAL.print(IMU.ay);  LOG_SERIAL.print(" ");
        // LOG_SERIAL.println(IMU.az);

        if (acceleration > SHAKE_THRESHOLD) {
            shakeDetect = true;
        } else {
            shakeDetect = false;
        }

        if (shakeState == 0) {
            // LOG_SERIAL.println("STATE 0");
            if (shakeDetect == true) {
                shakeState = 1;
                timer_shakeDebounce = 0;
                // LOG_SERIAL.println("STATE 1");
            }
        }

        if (shakeState == 1 && timer_shakeDebounce > shakeDebounceDelay) {
            // LOG_SERIAL.println("STATE 2");
            shakeState = 2;
        }

        if (shakeState == 2) {
            if (timer_shakeDebounce < shakeDebounceEnd) {
                if (shakeDetect) {
                    // LOG_SERIAL.println("SHAKEN, not stirred");
                    shaken = true;
                    timer_shakeRepeat = 0;
                    shakeState = 0;
                }
            } else {
                shakeState = 0;
                // LOG_SERIAL.println("Ended");
            }
        }

        // Serial.println(shaken);
    }
}

/********************

    ROTARY ENCODER


 ********************/
word val;
word prevVal;

bool compareMotorPosition(int currentAngle_in, int wantedAngle_in)
{
    bool anglesClose;
    if (currentAngle_in >= wantedAngle_in - ALLOWED_STEP_OFFSET && currentAngle_in <= wantedAngle_in + ALLOWED_STEP_OFFSET) {
        anglesClose = true;
    } else {
        anglesClose = false;
    }
    return anglesClose;
}

int getCurrentAngle()
{
#ifdef ANGLESENSOR
    //sometimes the rotary encoder gives a 0-reading (never twice in a row)
    //to prevent from using this strange value for calculation, read twice to make sure that the 0 is not an error
    val = angleSensor.getRawRotation();
    //  LOG_SERIAL.print("val = ");
    //  LOG_SERIAL.println(val);

    while (val == 0) {
        val = angleSensor.getRawRotation();
        //    LOG_SERIAL.print("checked again val = ");
        //    LOG_SERIAL.println(val);
    }

    int angle = ((val * 360.00) / 16384.00) - 360;

    //  LOG_SERIAL.print(
    //  LOG_SERIAL.print("angle = ");
    //  LOG_SERIAL.print(angle);
    //  LOG_SERIAL.print("\t");
    //  if (angleSensor.getErrors() > 0) {
    //    LOG_SERIAL.print("Errors: ");
    //    LOG_SERIAL.println(angleSensor.getErrors());
    //  }
    return angle;
#endif
}

/***************************

    Angle/Step Conversion

 ***************************/

int calculateShortestAngle(int a0, int a1)
{
    int max = 360;
    int da = (a1 - a0) % max;
    da = 2 * da % max - da;
    return da;
}

int convertStepsToDeg(int stepsReceived)
{
    int angle = int(receivedStep * 1.8);
    return angle;
}

float convertDegToSteps(int deg)
{
    float steps = ((float)deg / 360.0) * 200.0;
    return steps;
}

void recalibrateAccel()
{
    correctionPitch = IMU.pitch;
    correctionRoll = IMU.roll;
    recalibrated = true;
    // LOG_SERIAL.print("correctionPitch = ");
    // LOG_SERIAL.print(correctionPitch);
    // LOG_SERIAL.print("\t");
    // LOG_SERIAL.print("correctionRoll = ");
    // LOG_SERIAL.println(correctionRoll);
    // LOG_SERIAL.println("Recalibrated pitch and roll");
}

/***************************

          Stepper

 ***************************/
const int MAX_PULL_IN_SPEED = 10000;
const int MAX_RPM = 50000; //library can handle up to 300000, but motor cannot handle speeds over 4000
const int MAX_ACCEL = 45000; //library can handle up to 500000, motor cannot accelerate faster than 45000

void setPredefinedSpeedProfile(int speedProfileIndex)
{
    setNewSpeedProfile(speedProfiles[speedProfileIndex][0], speedProfiles[speedProfileIndex][1], speedProfiles[speedProfileIndex][2]);
}

void setNewSpeedProfile(int RPM_param, int acceleration_param, int pullInSpeed_param)
{
    if (pullInSpeed_param > MAX_PULL_IN_SPEED) {
        pullInSpeed_param = MAX_PULL_IN_SPEED;
    }

    if (RPM_param > MAX_RPM) {
        RPM_param = MAX_RPM;
    }

    if (pullInSpeed_param > MAX_PULL_IN_SPEED) {
        pullInSpeed_param = MAX_PULL_IN_SPEED;
    }

    motor.setAcceleration(acceleration_param);
    motor.setMaxSpeed(RPM_param);
    motor.setPullInSpeed(pullInSpeed_param);
}

void moveToStep(int newPos)
{
    if (onTable) {
        timer_check_if_held = 4000;
        givenMessage = false;
        onTable = false;
        LOG_SERIAL.println("OnTable is false through move");
        enableMotor();
    }
    motor.setTargetRel(newPos);
    controller.moveAsync(motor); //move to the new position (in steps)
}

void enableMotor()
{
    digitalWrite(MOT_EN_PIN, LOW);
}

void disableMotor()
{
    digitalWrite(MOT_EN_PIN, HIGH);
}

/**********************

    Effect Handling

 **********************/

void playNextMoveOfEffect()
{
    setPredefinedSpeedProfile(active_profiles[effect_play_index]);
    motor.setTargetRel(active_moves[effect_play_index] * MICRO_STEPS);
    controller.moveAsync(motor);
}

void updateSequences()
{
    effects_delay = 0;
    effect_play_index++;
}

void resetSequences()
{
    playingEffect = false;
    effect_play_index = 0;
}

void playEffect(int effectNumber)
{
    if (effectNumber == 202) {
        effect_wake_up(); //D200 - r001
    } else if (effectNumber == 203) { //R2
        effect_zoom_in(); //D010 - e001
        //    disableMotor();
        //subtle difference between zoom in and out is with the start of the animation, making a different sound
    } else if (effectNumber == 204) {
        effect_zoom_out(); //D010 - e002
    } else if (effectNumber == 205) { //L1
        effect_nudge();
    } else if (effectNumber == 206) {
        effect_confirm_preview(); //D100 - r001
    } else if (effectNumber == 207) {
        effect_celebrate(); //C100 - r002
    } else if (effectNumber == 208) {
        //    effect_thinking();
        effect_continuous_rotation_slow(); //S100 - r001
    } else if (effectNumber == 209) { //L2
        effect_spin();
    } else if (effectNumber == 210) {
        playingActiveEffect = true;
        effect_active();
    } else if (effectNumber == 211) {
        effect_accept_suggestion();
    } else if (effectNumber == 212) {
        effect_detailed_zoom_in();
    } else if (effectNumber == 213) {
        moveToZeroCCW();
    } else if (effectNumber == 214) {
        reset_held_timer();
    } else if (effectNumber == 215) {
        stopEffect();
    }
}

/********************

       Effects

********************/

void effect_wake_up()
{
    LOG_SERIAL.println("waking up");
    wantedAngle = 180;
    active_moves = wake_up_moves;
    active_delays = wake_up_delays;
    active_profiles = wake_up_profiles;
    active_effect_length = wake_up_length;
}

void effect_zoom_in()
{
    LOG_SERIAL.println("zooming in");
    active_moves = zoom_in_moves;
    active_delays = zoom_in_delays;
    active_profiles = zoom_in_profiles;
    active_effect_length = zoom_in_length;
}

void effect_zoom_out()
{
    LOG_SERIAL.println("zooming out");
    active_moves = zoom_out_moves;
    active_delays = zoom_out_delays;
    active_profiles = zoom_out_profiles;
    active_effect_length = zoom_out_length;
}

void effect_nudge()
{
    LOG_SERIAL.println("nudge");
    active_moves = nudge_moves;
    active_delays = nudge_delays;
    active_profiles = nudge_profiles;
    active_effect_length = nudge_length;
}

void effect_confirm_preview()
{
    LOG_SERIAL.println("confirm preview");
    active_moves = confirm_moves;
    active_delays = confirm_delays;
    active_profiles = confirm_profiles;
    active_effect_length = confirm_length;

    //move to the selected element
    //  wantedAngle = convertStepsToDeg(receivedStep) + ANGLE_OFFSET;
    currentAngle = getCurrentAngle() + ANGLE_OFFSET;
    deltaAngle = calculateShortestAngle(currentAngle, wantedAngle);
    confirm_moves[0] = convertDegToSteps(deltaAngle);

    int nudgeSteps = random(3, 10);
    confirm_moves[1] = nudgeSteps;
    confirm_moves[2] = -nudgeSteps;
}

void effect_celebrate()
{
    LOG_SERIAL.println("celebrating");
    wantedAngle = ANGLE_OFFSET;
    currentAngle = getCurrentAngle() + ANGLE_OFFSET;
    deltaAngle = calculateShortestAngle(currentAngle, wantedAngle);
    celebrate_moves[7] = convertDegToSteps(deltaAngle);

    active_moves = celebrate_moves;
    active_delays = celebrate_delays;
    active_profiles = celebrate_profiles;
    active_effect_length = celebrate_length;
}

void effect_accept_suggestion()
{
    LOG_SERIAL.println("accepting a suggestion");
    active_moves = accept_moves;
    active_delays = accept_delays;
    active_profiles = accept_profiles;
    active_effect_length = accept_length;
}

void effect_thinking()
{
    LOG_SERIAL.println("thinking");
    active_moves = thinking_moves;
    active_delays = thinking_delays;
    active_profiles = thinking_profiles;
    active_effect_length = thinking_length;
}

void effect_spin()
{
    LOG_SERIAL.println("spinning");
    active_moves = spin_moves;
    active_delays = spin_delays;
    active_profiles = spin_profiles;
    active_effect_length = spin_length;
}

void effect_continuous_rotation()
{
    LOG_SERIAL.println("continuous rotation");
    setPredefinedSpeedProfile(11);
    controller.rotateAsync(motor);
}

void effect_continuous_rotation_slow()
{
    LOG_SERIAL.println("continuous rotation slow");
    setPredefinedSpeedProfile(12);
    controller.rotateAsync(motor);
}

void effect_active()
{
    LOG_SERIAL.println("Active behaviour active");
    active_moves = active_behaviour_moves;
    active_delays = active_behaviour_delays;
    active_profiles = active_behaviour_profiles;
    active_effect_length = active_behaviour_length;
}

void effect_detailed_zoom_in()
{
    LOG_SERIAL.println("detailed zoom in");
    wantedAngle = ANGLE_OFFSET + 50;
    currentAngle = getCurrentAngle() + ANGLE_OFFSET;
    deltaAngle = calculateShortestAngle(currentAngle, wantedAngle);
    LOG_SERIAL.println(wantedAngle);
    LOG_SERIAL.println(deltaAngle);
    detailed_zoom_in_moves[1] = convertDegToSteps(deltaAngle);

    active_moves = detailed_zoom_in_moves;
    active_delays = detailed_zoom_in_delays;
    active_profiles = detailed_zoom_in_profiles;
    active_effect_length = detailed_zoom_in_length;
}

void reset_held_timer()
{
    LOG_SERIAL.println("reset the held timer through effect");
    timer_check_if_held = 0;
    active_moves = fake_moves;
    active_delays = fake_delays;
    active_profiles = fake_profile;
    active_effect_length = fake_lenght;
}

void moveToZeroCCW()
{
    LOG_SERIAL.println("CCW motion");
    wantedAngle = 0;
    currentAngle = getCurrentAngle() + ANGLE_OFFSET;
    //  deltaAngle = calculateShortestAngle(currentAngle, wantedAngle);
    //  LOG_SERIAL.print("currentAngle = ");
    //  LOG_SERIAL.print(currentAngle);
    //  LOG_SERIAL.print("wantedAngle = ");
    //  LOG_SERIAL.println(wantedAngle);
    //  LOG_SERIAL.print("move steps = ");
    //  LOG_SERIAL.println(-int(convertDegToSteps(currentAngle)));
    //  moveToStep(-int(convertDegToSteps(currentAngle)));
    CCW_moves[0] = -int(convertDegToSteps(currentAngle));

    active_moves = CCW_moves;
    active_profiles = CCW_profile;
    active_delays = fake_delays;
    active_effect_length = 1;
}

void stopEffect()
{
    LOG_SERIAL.println("STOP EFFECT");
    controller.stopAsync();
}
