#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__

#include <Arduino.h>

#define HIKEY_SERIAL Serial1
#define LOG_SERIAL   Serial
#define IMU_INT_PIN  20       
#define ENC_CS_PIN   14       

#define PCB_REVISION 2

#if PCB_REVISION == 1

#define MOT_EN_PIN   2
#define MOT_STEP_PIN 7        
#define MOT_DIR_PIN  8        

#define MOT_FLT_PIN  5
#define MOT_SLP_PIN  6

#define MOT_M0_PIN   3 
#define MOT_M1_PIN   4

#elif PCB_REVISION == 2

#define MOT_EN_PIN   8
#define MOT_STEP_PIN 4         
#define MOT_DIR_PIN  5        

#define MOT_FLT_PIN  2
#define MOT_SLP_PIN  3

#define MOT_M0_PIN   7 
#define MOT_M1_PIN   6

#else

#error "unknown PCB revision"

#endif

void floatToBytes(float val, byte* bytes_array);

void sendDataToHikey(byte accelX[4], byte accelY[4], int inPosition, int onTable, int shaken);

#endif

