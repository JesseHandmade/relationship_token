#include <Servo.h>
#include <elapsedMillis.h>

Servo myservo;  // create servo object to control a servo
elapsedMillis timer_servo;
elapsedMillis timer_random_slider;
elapsedMillis timer_video;

const int enA = 5;
const int in1 = 3;
const int in2 = 8;
const int SLIDER_PIN = A1;

bool manualInput = false;     //true = serial input, false = play effects

int servo_interval = 15;
bool goingForward = true;

int pos = 0;    // variable to store the servo position
int newPos = -1;
int previousPos = -2;
int sliderPos = -1;
boolean moveNow = false;


void setup() {
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(SLIDER_PIN, INPUT);
  myservo.attach(6);  // attaches the servo on pin 9 to the servo object
  Serial.begin(9600);
  timer_servo = 0;
}

void loop() {
  if (Serial.available()) {
    String incomingReading = Serial.readString();
    Serial.print(incomingReading);
    //    newPos = incomingReading.toInt();
    //    constrain(newPos, 0, 1024);
    Serial.print("\t");
    Serial.println(newPos);
  }

  if (timer_video < 10000) {
    moveServoDegreesFB(90, 6000);  //move forward and backward (degrees/lenght)

    if (timer_random_slider > 4000) {
      newPos = random(100, 650);
      timer_random_slider = 0;
    }

    if (moveNow) {
      moveToPos(newPos, 200);
    }

    //reset bool if new position found
    if (previousPos != newPos) {
      moveNow = true;
    }
    //reset position so it only moves when it has not reached the wanted position
    previousPos = newPos;
  } else {
    moveServoRandom();//move forward and backward (degrees/lenght)

    if (timer_random_slider > 2000) {
      newPos = random(100, 450);
      timer_random_slider = 0;
    }

    if (moveNow) {
      moveToPos(newPos, 200);
    }

    //reset bool if new position found
    if (previousPos != newPos) {
      moveNow = true;
    }
    //reset position so it only moves when it has not reached the wanted position
    previousPos = newPos;
  }
}

//void moveServo(int requestedPos) {
//
//  if (timer_servo > SERVO_INTERVAL) {
//    timer_servo = 0;
//    myservo.write(requestedPos);
//  }
//}

bool goForward = true;

void moveServoDegreesFB(int deg, int lenght) {
  servo_interval = lenght / deg; //calculate time for 1 step, based on lenght and degrees

  //move to wanted degrees based on time, move back when degrees are reached
  if (timer_servo > servo_interval) {
    timer_servo = 0;
    if (goForward) {
      pos++;
      if (pos >= deg) {
        goForward = false;
      }
    } else {
      pos--;
      if (pos <= 0) {
        goForward = true;
      }
    }
    myservo.write(pos);
  }
}



void moveServoRandom() {

  if (timer_servo > random(300,600)) {
    timer_servo = 0;
    pos = random(0, 90);
    myservo.write(pos);
  }
}


void moveToPos(int wantedPos, int wantedSpeed) {
  //move forward/backward or stop once destination has been reached
  if (sliderPos > wantedPos + 5) {
    moveBackward();
    setMotorSpeed(wantedSpeed);
    readSliderPos();

  } else if (sliderPos < wantedPos - 5) {
    moveForward();
    setMotorSpeed(wantedSpeed);
    readSliderPos();

  } else {
    stopMotor();
    moveNow = false;
    readSliderPos();
  }
}


/*-------------------------------------------------------

                  LINEAR MOTOR FUNCTIONS

  -------------------------------------------------------*/
void moveForward() {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
}

void moveBackward() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
}

void stopMotor() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
}

void setMotorSpeed(int wantedSpeed) {     //0-255
  analogWrite(enA, wantedSpeed);
}

void readSliderPos() {
  sliderPos = analogRead(SLIDER_PIN);
  Serial.println(sliderPos);
}



