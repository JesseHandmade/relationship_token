
// Example by Tom Igoe

import processing.serial.*;
int[] incomingBytes; 
Serial myPort;  // The serial port
byte[] accelXArray;

void setup() {
  // List all the available serial ports
  printArray(Serial.list());
  // Open the port you are using at the rate you want:
  myPort = new Serial(this, Serial.list()[3], 19200);
}

void draw() {
  // Expand array size to the number of bytes you expect
  byte[] inBuffer = new byte[9];
  while (myPort.available() >= 8) {
    inBuffer = myPort.readBytes();
    myPort.readBytes(inBuffer);
    if (inBuffer != null) {
      //for(int i = 0; i<9; i++){
      //   println(inBuffer[i]); 
      //}
      //println("DONE"); 
      String myString = new String(inBuffer);
      println(myString);
    }
  }
}