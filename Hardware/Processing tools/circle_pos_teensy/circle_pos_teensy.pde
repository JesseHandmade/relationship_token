
import processing.serial.*;
import controlP5.*;

// The serial port:
Serial port;  
ControlP5 cp5;
ControlP5 slider1;

String inString;
int lf = 10;      // ASCII linefeed 
int acceleration = 50;
int pullInSpeed = 50;
int RPM = 120;
int extraParameter = 5;

void setup() {
  size(1060, 960);
  background(0);

  String[] ports = Serial.list();
  String portName = "";
  for (int i =0; i < ports.length; i++) {
    portName = ports[i];
    if (portName.indexOf("cu.usbmodem") > -1) break;
  }

  println(portName);

  // Open the port you are using at the rate you want:
  port = new Serial(this, portName, 115200);
  port.bufferUntil(lf);

  //add slider at wantedPosition
  cp5 = new ControlP5(this);

  cp5.addSlider("acceleration")
    .setPosition(100, 50)
    .setRange(1, 250)
    .setHeight(40)
    .setWidth(300)
    .setDecimalPrecision(2) 
    ;

  cp5.getController("acceleration").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("acceleration").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);

  cp5.addSlider("pullInSpeed")
    .setPosition(100, 120)
    .setRange(1, 100)
    .setHeight(40)
    .setWidth(300)
    .setDecimalPrecision(2) 
    ;
  cp5.getController("pullInSpeed").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("pullInSpeed").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);

  cp5.addSlider("RPM")
    .setPosition(660, 50)
    .setRange(10, 250)
    .setHeight(40)
    .setWidth(300)
    .setDecimalPrecision(2) 
    ;
  cp5.getController("RPM").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("RPM").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);

  cp5.addSlider("extraParameter")
    .setPosition(660, 120)
    .setRange(1, 500)
    .setHeight(40)
    .setWidth(300)
    .setDecimalPrecision(2) 
    ;
  cp5.getController("extraParameter").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("extraParameter").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);



  int firstButtonX = 40;
  int firstButtonY = 850;
  // create a series of buttons
  cp5.addButton("wake_up")
    .setPosition(firstButtonX, firstButtonY)
    .setSize(80, 80)
    ;

  cp5.addButton("zoom_IN")
    .setPosition(firstButtonX+100, firstButtonY)
    .setSize(80, 80)
    ;

  cp5.addButton("zoom_OUT")
    .setPosition(firstButtonX+200, firstButtonY)
    .setSize(80, 80)
    ;

  cp5.addButton("nudge")
    .setPosition(firstButtonX+300, firstButtonY)
    .setSize(80, 80)
    ;

  cp5.addButton("confirm_preview")
    .setPosition(firstButtonX+400, firstButtonY)
    .setSize(80, 80)
    ;

  cp5.addButton("celebrate")
    .setPosition(firstButtonX+500, firstButtonY)
    .setSize(80, 80)
    ;

  cp5.addButton("thinking")
    .setPosition(firstButtonX+600, firstButtonY)
    .setSize(80, 80)
    ;

  cp5.addButton("spin")
    .setPosition(firstButtonX+700, firstButtonY)
    .setSize(80, 80)
    ;

  cp5.addButton("rotate")
    .setPosition(firstButtonX+800, firstButtonY)
    .setSize(80, 80)
    ;
    
  cp5.addButton("accept_suggestion")
    .setPosition(firstButtonX+900, firstButtonY)
    .setSize(80, 80)
    ;
}

int myValue = 10;

float circleX;
float circleY;
float angle;

void draw() {
  background(0);
  smooth();
  strokeWeight(0);
  fill(76, 34, 255);
  //noFill();
  ellipse(width/2, height/2, 400, 400);
  fill(0);
  ellipse(width/2, height/2, 300, 300);
  ellipse(circleX, circleY, 40, 40);
}

void serialEvent(Serial p) { 
  inString = p.readStringUntil(lf); 
  println(inString);
} 

/********************  
 INTERFACING
 ********************/


void mouseReleased() {
  //check if the mouseRelease happened within the circle
  if (dist(mouseX, mouseY, width/2, height/2) < 400/2) {
    angle = atan2(mouseY - height/2, mouseX - width/2); 
    circleX = mouseX;
    circleY = mouseY;
    angle = (int)degrees(angle);    //convert from radians to degrees  

    //initial value 
    if (angle<0) {
      angle +=360;
    }

    //convert to steps
    angle = (angle/360)*200;
    //print((int)angle);
    //print("\t");
    //print(acceleration);
    //print("\t");
    //println(deceleration);

    out[0] = byte(angle);
    print("angle = ");
    println(out[0]);
    out[1] = byte(acceleration);
    out[2] = byte(pullInSpeed);

    //sendSingleMessage();
    sendArrayMessage();
    //port.write(byte(sliderValue));
  }
}


public void wake_up() {
  println("Wakie wakie");
  out[0] = byte(202);
  sendArrayMessage();      //send to arduino
}

public void zoom_IN() {
  println("You pressed button B");
  out[0] = byte(203);
  sendArrayMessage();      //send to arduino
}


public void zoom_OUT() {
  println("You pressed button C");
  out[0] = byte(204);
  sendArrayMessage();      //send to arduino
}

public void nudge() {
  println("Nudge");
  out[0] = byte(205);
  sendArrayMessage();      //send to arduino
}

public void confirm_preview() {
  println("confirm Preview");
  out[0] = byte(206);
  sendArrayMessage();      //send to arduino
}


public void celebrate() {
  println("PARTY TIME");
  out[0] = byte(207);
  sendArrayMessage();      //send to arduino
}

public void thinking() {
  println("thinking...");
  out[0] = byte(208);
  sendArrayMessage();      //send to arduino
}

public void spin() {
  println("spinning...");
  out[0] = byte(209);
  sendArrayMessage();      //send to arduino
}

public void rotate() {
  println("rotating");
  out[0] = byte(210);
  sendArrayMessage();      //send to arduino
}

public void accept_suggestion() {
  println("rotating");
  out[0] = byte(211);
  sendArrayMessage();      //send to arduino
}

/********************  
 COMMUNICATION
 ********************/

void sendSingleMessage() {
  port.write(byte(angle));
}

byte out[] = new byte[5];

void sendArrayMessage() {
  //send the values to the arduino app
  out[1] = byte(acceleration);
  out[2] = byte(pullInSpeed);
  out[3] = (byte)RPM;
  out[4] = byte(extraParameter);
  port.write(out);
  //println(out);
}